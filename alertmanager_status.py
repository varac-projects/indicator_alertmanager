#!/usr/bin/env python3
"""Alertmanager status applet using appindicator."""

import json
from pathlib import Path

import plac
import yaml
from query_alertmanager import get_alerts


def show_alerts(opts: list) -> str:
    """Show alerts."""
    alerts = get_alerts(url=opts["url"], user=opts["user"], password=opts["password"])

    # Construct waybar output
    # https://github.com/Alexays/Waybar/wiki/Module:-Custom#return-type
    # {"text": "$text", "alt": "$alt", "tooltip": "$tooltip",
    #  "class": "$class", "percentage": $percentage }.
    # This means the output should also be on a single line
    output = {}
    output["text"] = alerts["count"]
    if alerts["summary"] == "Cant connect":
        output["alt"] = "error"
        output["tooltip"] = "Some error occurred."
    elif alerts["count"] > 0:
        output["alt"] = "firing"
        output["tooltip"] = "\n".join(alerts["alerts"])
    else:
        output["alt"] = "snafu"
        output["tooltip"] = "Situation normal, all fucked up 😃"
    print(json.dumps(output))


@plac.opt("configpath", "Path to alertmanager config", type=Path)
@plac.opt("mode", "Mode", type=str, choices=["waybar", "indicator"])
# plac.flg('debug', "Enable debug mode")
def main(
    configpath: str = "~/.config/alertmanager_status/config.yml", mode: str = "wayland"
) -> None:
    """Run main function."""
    # Load config
    with Path.expanduser(configpath).open() as config_file:
        opts_raw = yaml.safe_load(config_file)
        opts = {}
        opts["url"] = opts_raw.get("url")
        opts["port"] = opts_raw.get("port", 443)
        opts["user"] = opts_raw.get("user", "admin")
        opts["password"] = opts_raw.get("password")
        opts["icon_red"] = Path("./prometheus.svg").resolve()
        opts["icon_green"] = Path("./prometheus-green.svg").resolve()

    if mode == "wayland":
        show_alerts(opts)
    # if mode == "indicator":
    # Indicator()


if __name__ == "__main__":
    plac.call(main)
